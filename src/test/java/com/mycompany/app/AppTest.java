package com.mycompany.app;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import com.mycompany.app.JavaHTTPServer;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;



/**
 * Unit test for simple App.
 */
public class AppTest {

    /**
    This tests needs the server to run to work.
    Previous attempts to start the server would break that application
     */
    @Test
    public void seleniumTestIndex() {
        //setting the driver executable
        //System.setProperty("webdriver.chrome.driver", "Driver/chromedriver.exe");
        // WebDriverManager.chromedriver().setup();
        // ChromeOptions options = new ChromeOptions();
        // options.setHeadless(true);
        WebDriver driver = new HtmlUnitDriver();

        //Applied wait time
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //open browser with desried URL
        driver.get("http://localhost:8080");

        WebElement welcome = driver.findElement(By.cssSelector("h1"));

        assertEquals(welcome.getAttribute("textContent"), "Hello World");
       
        System.out.println("************Found Element:************");
        System.out.println(welcome.getAttribute("textContent"));
        //closing the browser
        driver.close();
    }
    
    @Test
    public void seleniumTest404() {
        //setting the driver executable
        //System.setProperty("webdriver.chrome.driver", "Driver/chromedriver.exe");
        // WebDriverManager.chromedriver().setup();
        // ChromeOptions options = new ChromeOptions();
        // options.setHeadless(true);
        WebDriver driver = new HtmlUnitDriver();

        //Applied wait time
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //open browser with desried URL
        driver.get("http://localhost:8080/bad-page");

        WebElement welcome = driver.findElement(By.cssSelector("h1"));

        assertEquals(welcome.getAttribute("textContent"), "404 Not Found");
       
        System.out.println("************Found Element:************");
        System.out.println(welcome.getAttribute("textContent"));
        //closing the browser
        driver.close();
    }
}
