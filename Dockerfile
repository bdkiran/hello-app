#based docker image off of:
#https://stackoverflow.com/questions/27767264/how-to-dockerize-maven-project-and-how-many-ways-to-accomplish-it
#Build Stage
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY Webpages /home/app/Webpages
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean -DskipTests=true package

#Package stage
FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine
COPY --from=build /home/app/Webpages /usr/local/lib/Webpages
COPY --from=build /home/app/target/hello-app-1.0-SNAPSHOT.jar /usr/local/lib/hello-app.jar
EXPOSE 8080
WORKDIR /usr/local/lib
ENTRYPOINT ["java","-jar","hello-app.jar"]